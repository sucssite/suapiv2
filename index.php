<?php
/*
* SUCS SU-APIv2
* Imran Hussain - imranh@sucs.org
* Stuart John Watson - ripp_@sucs.org
*
* Use to lookup SUCS membership data from the Swansea University Students Union
* website. curls the SU site for a login cookie using the details provided in
* loginsetails.php. Once it has that it generates the time range for the current
* academic year. It then takes the specfied $orgid from the url params (SUCS
* is 6613) and does another curl to the report viewer page on the SU site.
* We then scrape that page for some data to make another curl request.
* The SU site then finaly returns XML, we then turn that into json and spit it
*  out.
*/

// We want errors! When do we want them? Now!
error_reporting(E_ALL);
ini_set('display_errors', 1);

// If there isn't api key then die
if (isset($_GET['apikey']) == FALSE){
	die("Please provide an api key");
}

// Get the api key the user is trying to use
$apikey = $_GET['apikey'];

// Import the array of api keys called $apikeys
include_once "../apikeys.php";

// If it's not valid then DIE DIE DIE
if ( in_array($apikey,$apikeys) == FALSE) {
	die("Invalid api key");
}

// If there isn't an orgid then die
if (isset($_GET['orgid']) == FALSE){
        die("Please provide an orgid");
}

// Get the api key the user is trying to use
$orgid = $_GET['orgid'];


// If they get here then they are allowed to be here

// Get the $BASEURL, $USERNAME and $PASSWORD from a seprate file
include "../logindetails.php";

// SU website is built using ASP.NET which is a kinda ok-ish
// we need to scrape the login page and steal the __VIEWSTATE var then
// post it back to it, it's basically a checksum+data of the entire page
$ch0 = curl_init($BASEURL."/login/");
curl_setopt($ch0,CURLOPT_COOKIEJAR, "../logincookies");
curl_setopt($ch0,CURLOPT_FAILONERROR,TRUE);
curl_setopt($ch0,CURLOPT_RETURNTRANSFER,TRUE);
curl_setopt($ch0,CURLOPT_HEADER,TRUE);
$loginhtml = curl_exec($ch0);
curl_close($ch0);

// Mad regex I stole off the internet to get the viewstate
preg_match('/__VIEWSTATE\" value=\"(.*)\"/i', $loginhtml, $matches);
$LOGINVIEWSTATE = rawurlencode($matches[1]);

// Get the login cookie and store it as a file called cookies
$ch1 = curl_init($BASEURL."/login/");
curl_setopt($ch1,CURLOPT_COOKIEFILE, "../logincookies"); //read from exisiting cookies
curl_setopt($ch1,CURLOPT_COOKIEJAR, "../logedincookies"); //write to new cookie file
curl_setopt($ch1,CURLOPT_FAILONERROR,TRUE);
curl_setopt($ch1,CURLOPT_RETURNTRANSFER,TRUE); //Used to supress body output, tried with just HEAD request but that failed
curl_setopt($ch1,CURLOPT_HEADER,TRUE);
curl_setopt($ch1,CURLOPT_POSTFIELDS,
    "__EVENTTARGET=" .
    "&__EVENTARGUMENT=" .
    "&__VIEWSTATE=" . $LOGINVIEWSTATE .
    "&__VIEWSTATEGENERATOR=7CD7556D" .
    "&ctl00%24logincontrol%24UserName=" . $USERNAME .
    "&ctl00%24logincontrol%24Password=" . $PASSWORD .
    "&ctl00%24logincontrol%24btnLogin=Log+In"
);
$loginhtml = curl_exec($ch1);
curl_close($ch1);

// Stuff for generating the dates
$date = getdate(time());
// Anything before September is the previous academic year
if ($date['mon'] < 9) {
	$loweryear = $date['year'] - 1;
	$upperyear = $date['year'];
} else {
	$loweryear = $date['year'];
	$upperyear = $date['year'] + 1;
}


// SU website is built using ASP.NET which is a kinda ok-ish
// we need to scrape the page and steal the __VIEWSTATE var then
// post it back to it, it's basically a checksum+data of the entire page
$ch2 = curl_init($BASEURL."/organisation/salesreports/${orgid}/");
curl_setopt($ch2,CURLOPT_COOKIEFILE, "../logedincookies"); //get the right session id to look at the page
curl_setopt($ch2,CURLOPT_COOKIEJAR, "../reportCSRFcookies"); //save the new cookies with the anticsrf data to a new file
curl_setopt($ch2,CURLOPT_FAILONERROR,TRUE);
curl_setopt($ch2,CURLOPT_RETURNTRANSFER,TRUE);
curl_setopt($ch2,CURLOPT_HEADER,TRUE);
$reportCSRFhtml = curl_exec($ch2);
curl_close($ch2);

// Mad regex I stole off the internet to get the viewstate
preg_match('/__VIEWSTATE\" value=\"(.*)\"/i', $reportCSRFhtml, $matches);
$REPORTVIEWSTATE = rawurlencode($matches[1]);

$ch3 = curl_init($BASEURL."/organisation/salesreports/${orgid}/");
curl_setopt($ch3,CURLOPT_COOKIEFILE, "../reportCSRFcookies");
curl_setopt($ch3,CURLOPT_FAILONERROR,TRUE);
curl_setopt($ch3,CURLOPT_RETURNTRANSFER,TRUE);
curl_setopt($ch3,CURLOPT_POSTFIELDS,
    "__EVENTTARGET=ctl00%24ctl00%24Main%24AdminPageContent%24lbPurchasers" .
    "&__EVENTARGUMENT=" .
    "&__VIEWSTATE=" . $REPORTVIEWSTATE .
    "&__VIEWSTATEGENERATOR=9B3E427D" .
    "&ctl00%24ctl00%24Main%24AdminPageContent%24drDateRange%24txtFromDate=01%2F09%2F".$loweryear .
    "&ctl00%24ctl00%24Main%24AdminPageContent%24drDateRange%24txtFromTime=00%3A00" .
    "&ctl00%24ctl00%24Main%24AdminPageContent%24drDateRange%24txtToDate=31%2F08%2F".$upperyear .
    "&ctl00%24ctl00%24Main%24AdminPageContent%24drDateRange%24txtToTime=00%3A00" .
    "&ctl00%24ctl00%24Main%24AdminPageContent%24ReportViewer1%24ctl09%24VisibilityState%24ctl00=ReportPage"
);
$html = curl_exec($ch3);
curl_close($ch3);

//This is kind of like stopping someone who was using a big hammer and giving them a slightly better hammer.
//(It should be more robust though)
$dom = new DOMDocument();
@$dom->loadHTML($html);
foreach($dom->getElementsByTagName("script") as $script){
    foreach($script->childNodes as $node) {
        if ($node->nodeType != XML_CDATA_SECTION_NODE) {
            echo $node->nodeType,XML_TEXT_NODE;
            continue;
        }
        if (preg_match('/(?<="ExportUrlBase":")[^"]+/',$node->data,$match)){
            $exportUrlBase = str_replace("\\u0026","&",$match[0]);
        }
    }
}

$ch4 = curl_init($BASEURL."${exportUrlBase}XML");
curl_setopt($ch4,CURLOPT_COOKIEFILE, "../reportCSRFcookies");
curl_setopt($ch4,CURLOPT_FAILONERROR,TRUE);
curl_setopt($ch4,CURLOPT_RETURNTRANSFER,TRUE);
$xml = curl_exec($ch4);
curl_close($ch4);

echo json_encode(@simplexml_load_string($xml)); //I hope this works consisentally

//Clean up cookies, just to be safe
unlink("../logincookies");
unlink("../logedincookies");
unlink("../reportCSRFcookies");
?>
